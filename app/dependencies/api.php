<?php

$container = $app->getContainer();

$container['api_response_array'] = function ($c) {
    return new \Ptx\Api\Response\ArrayResponse();
};

$container['controller_api'] = function ($c) {
    return new \Ptx\Api\Controller\ApiController(
        $c['factory_api_request'],
        $c['api_response_array']
    );
};

$container['factory_api_request'] = function ($c) {
    return new \Ptx\Api\Factory\ApiRequestFactory();
};

$container['api_handler_mails_send'] = function ($c) {
    return new \Ptx\Api\Handlers\MailsSendHandler(
        $c['mailer_mail_service']
    );
};
