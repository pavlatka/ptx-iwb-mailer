<?php

$container['mailer_service_mailer'] = function ($c) {
    return new \Ptx\Mailer\Service\MailerService();
};

$container['mailer_smtp_dao'] = function ($c) {
    return new \Ptx\Mailer\Dao\SmtpDao(
        $c['db_connection']
    );
};

$container['mailer_smtp_factory'] = function ($c) {
    return new \Ptx\Mailer\Factory\SmtpFactory($c);
};

$container['mailer_smtp_repository'] = function ($c) {
    return new \Ptx\Mailer\Repository\SmtpRepository(
        $c['mailer_smtp_dao'],
        $c['mailer_smtp_factory'],
        $c['base_identity_mapper']
    );
};

$container['mailer_user_dao'] = function ($c) {
    return new \Ptx\Mailer\Dao\UserDao(
        $c['db_connection']
    );
};

$container['mailer_lazy_entity_factory'] = function ($c) {
    return new \Ptx\Mailer\Factory\LazyEntityFactory($c);
};

$container['mailer_user_factory'] = function ($c) {
    return new \Ptx\Mailer\Factory\UserFactory(
        $c['mailer_lazy_entity_factory']
    );
};

$container['mailer_user_repository'] = function ($c) {
    return new \Ptx\Mailer\Repository\UserRepository(
        $c['mailer_user_dao'],
        $c['mailer_user_factory'],
        $c['base_identity_mapper']
    );
};

$container['mailer_user_service'] = function ($c) {
    return new \Ptx\Mailer\Service\UserService(
        $c['mailer_user_repository'],
        $c['mailer_smtp_repository']
    );
};

$container['mailer_mail_service'] = function ($c) {
    return new \Ptx\Mailer\Service\MailerService(
        $c['mailer_user_service'],
        $c['mailer_message_log_repository']
    );
};

$container['mailer_message_log_dao'] = function ($c) {
    return new \Ptx\Mailer\Dao\MessageLogDao(
        $c['db_connection']
    );
};

$container['mailer_message_log_repository'] = function ($c) {
    return new \Ptx\Mailer\Repository\MessageLogRepository(
        $c['mailer_message_log_dao']
    );
};
