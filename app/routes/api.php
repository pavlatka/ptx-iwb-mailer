<?php

use \Slim\Http\Request;
use \Slim\Http\Response;
use \Ptx\Api\Handlers\CallHandler;

$app->group('/v1', function () use ($app) {

    $app->get('/mails', function (Request $request, Response $response, $args) {
        return 'Silence is still golden!';
    });

    $app->post('/mails', function (Request $request, Response $response, $args) {
        $actionName = 'mails_send';
        $authToken  = $request->getHeader('Authorization');
        $params     = array_merge($request->getParams(), $args, array('version' => 1));

        $controller  = $this->controller_api;
        $handler     = $this->api_handler_mails_send;
        $apiResponse = $this->api_response_array;

        $apiRequest   = $controller->prepareApiRequest($authToken, $actionName, $params);
        $data         = $controller->process($apiRequest, $handler);
        $responseData = $apiResponse->response($data);

        return $response->withJson($responseData['data'], $responseData['code']);
    });
});
