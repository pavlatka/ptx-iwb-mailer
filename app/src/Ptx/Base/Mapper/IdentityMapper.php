<?php
namespace Ptx\Base\Mapper;

class IdentityMapper
{
    private $repository = array();

    /**
     * Adds new entry into the mapper
     *
     * @param string $key - unique identifier to find entity later on
     * @param mixed $entity
     */
    public function addToRepository($key, $entity)
    {
        $this->repository[$key] = $entity;
    }

    /**
     * Checks whether the key already exists in the repository
     *
     * @param string $key
     *
     * @return bool
     */
    public function isInRepository($key)
    {
        return (bool)array_key_exists($key, $this->repository);
    }

    /**
     * Retrieved data from the local repository
     *
     * @param string $key - unique identifier we used when adding entry to repository
     *
     * @return mixed
     */
    public function getFromRepository($key)
    {
        if ($this->isInRepository($key) === false) {
            return null;
        }

        return $this->repository[$key];
    }
}
