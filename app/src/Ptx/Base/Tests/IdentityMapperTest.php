<?php
namespace Ptx\Base\Tests;

use Ptx\Base\Mapper\IdentityMapper;

class IdentityMapperTest extends \PHPUnit_Framework_TestCase
{
    private $mapper;

    protected function setUp()
    {
        $this->mapper = new IdentityMapper();
    }

    protected function tearDown()
    {
        unset($this->mapper);
    }

    public function testIsInRepositoryReturnFalsefNotInRepository()
    {
        $key = 'test';

        $inRepository = $this->mapper->isInRepository($key);

        $this->assertFalse($inRepository);
    }

    public function testIsInRepositoryReturnValidEntityFromRepository()
    {
        $key    = 'test';
        $entity = new \stdClass();

        $this->mapper->addToRepository($key, $entity);

        $inRepository = $this->mapper->isInRepository($key);
        $this->assertTrue($inRepository);
    }

    public function testAddToRepositoryStoreDataProperly()
    {
        $key = 'test';
        $entity = new \stdClass();

        $this->mapper->addToRepository($key, $entity);

        $result = $this->mapper->getFromRepository($key);
        $isValid = $result instanceof \stdClass;

        $this->assertTrue($isValid);
    }

    public function testGetFromRepositoryReturnsNullInCaseKeyDoesNotExist()
    {
        $this->assertEquals(null, $this->mapper->getFromRepository('key'));
    }
}
