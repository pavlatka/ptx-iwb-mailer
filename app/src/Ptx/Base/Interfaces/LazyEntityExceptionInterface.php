<?php
namespace Ptx\Base\Interfaces;

interface LazyEntityExceptionInterface
{
    const ERROR_BAD_REQUEST    = 400;
    const ERROR_UNKNOWN_ENTITY = 401;
    const ERROR_RUNTIME_ERROR  = 500;
}
