<?php
namespace Ptx\Base\Interfaces;

interface RepositoryExceptionInterface
{
    const ERROR_BAD_REQUEST   = 400;
    const ERROR_NOT_FOUND     = 404;
    const ERROR_RUNTIME_ERROR = 500;
}
