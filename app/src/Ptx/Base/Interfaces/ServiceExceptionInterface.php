<?php
namespace Ptx\Base\Interfaces;

interface ServiceExceptionInterface
{
    const ERROR_RUNTIME_ERROR = 500;
}
