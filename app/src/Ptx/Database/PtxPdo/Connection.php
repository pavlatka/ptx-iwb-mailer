<?php namespace Ptx\Database\PtxPdo;

use Ptx\Database\PtxPdo\Query;
use Ptx\Database\Interfaces\ResultInterface;
use Ptx\Database\Interfaces\ConnectionInterface;

class Connection implements ConnectionInterface
{
    private $result;

    public function __construct(ResultInterface $result)
    {
        $this->result = $result;
    }

    /**
     * Query for the dabatabase.
     *
     * @param string $sql - sql to be processed.
     * @param array  $data - data for the query
     *
     * @return Result
     */
    public function query($sql, array $data = array())
    {
        $query = new Query();
        $query
            ->setSql($sql)
            ->setData($data);

        $this->result->setQuery($query);

        return $this->result;
    }

    /**
     * Possible to insert data easily
     *
     * @param string $table - name of the table
     * @param array  $data - data to be inserted
     * @param string $duplicateUpdate - extra part in case we need ON DUPLICATE KEY UPDATE
     *
     * @return Result
     */
    public function insert($table, array $data, $duplicateUpdate = null)
    {
        if (count($data) === 0) {
            throw new DatabaseException(
                'Data cannot be empty array'
            );
        }

        $values = $queryData = array();
        foreach ($data as $key => $value) {
            $value  = trim($value);
            $sqlKey = ':' . $key;

            $values[] = $sqlKey;
            $queryData[$sqlKey] = $value;
        }

        $sql = 'insert into [' . $table . '] (' . implode(',', array_keys($data)) . ')';
        $sql .= ' values (' . implode(',', $values) . ')';

        if ($duplicateUpdate !== null) {
            if (is_array($duplicateUpdate)) {
                $duplicateUpdate = $this->buildDuplicateKeyUpdate($duplicateUpdate);
            }

            $sql .= ' on duplicate key update ' . $duplicateUpdate;
        }

        return $this->query($sql, $queryData);
    }

    /**
     * Builds the part of an sql which follows on duplicate key update
     *
     * @param array $data - data to be built from
     *
     * @return string
     */
    protected function buildDuplicateKeyUpdate(array $data)
    {
        $string = null;

        foreach ($data as $column) {
            $string .= $column . ' = values(' . $column . '),';
        }

        return substr($string, 0, -1);
    }

    /**
     * Feature to update data in a table
     *
     * @param string $table - name of the table
     * @param array  $data - data to be updated
     * @param array  $conditions - condition to specify which rows should be update
     *
     * @return Result
     */
    public function update($table, array $data, array $conditions)
    {
        if (count($data) === 0) {
            throw new DatabaseException(
                'Data cannot be empty array'
            );
        } elseif (count($conditions) === 0) {
            throw new DatabaseException(
                'Conditions cannot be empty array'
            );
        }

        $queryData = array();
        $sets      = array();
        foreach ($data as $key => $value) {
            $value  = trim($value);
            $sqlKey = ':' . $key;

            $queryData[$sqlKey]  = $value;

            $sets[]   = $key . '=' . $sqlKey;
        }

        $sql = 'update [' . $table . '] set ' . implode(',', $sets) . ' where 1';

        $values = array();
        foreach ($conditions as $key => $value) {
            $value  = trim($value);
            $sqlKey = ':cond_' . $key;

            $queryData[$sqlKey]  = $value;

            $sql .= ' AND  ' . $key . '=' . $sqlKey;
        }

        return $this->query($sql, $queryData);
    }

    /**
     * Feature to delete data from a table
     *
     * @param string $table - name of the table
     * @param array  $conditions - condition to specify which rows should be delete
     *
     * @return Result
     */
    public function delete($table, array $conditions)
    {
        if (count($conditions) === 0) {
            throw new DatabaseException(
                'Conditions cannot be empty array'
            );
        }

        $sql = 'delete from [' . $table . '] where 1';

        $values = array();
        foreach ($conditions as $key => $value) {
            $value  = trim($value);
            $sqlKey = ':cond_' . $key;

            $queryData[$sqlKey]  = $value;

            $sql .= ' AND  ' . $key . '=' . $sqlKey;
        }

        return $this->query($sql, $queryData);
    }
}
