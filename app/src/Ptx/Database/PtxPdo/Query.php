<?php namespace Ptx\Database\PtxPdo;

use Ptx\Database\Interfaces\QueryInterface;

class Query implements QueryInterface
{
    private $sql;
    private $data;
    private $limit;
    private $offset;
    private $orderBy;
    private $groupBy;

    public function setSql($sql)
    {
        $this->sql = $sql;

        return $this;
    }

    public function getSql()
    {
        return $this->sql;
    }

    public function setLimit($limit)
    {
        $this->limit = $limit;

        return $this;
    }

    public function getLimit()
    {
        return $this->limit;
    }

    public function setOffset($offset)
    {
        $this->offset = $offset;

        return $this;
    }

    public function getOffset()
    {
        return $this->offset;
    }

    public function setOrderBy($orderBy)
    {
        $this->orderBy = $orderBy;

        return $this;
    }

    public function getOrderBy()
    {
        return $this->orderBy;
    }

    public function setGroupBy($groupBy)
    {
        $this->groupBy = $groupBy;

        return $this;
    }

    public function getGroupBy()
    {
        return $this->groupBy;
    }

    public function setData($data)
    {
        $this->data = $data;

        return $this;
    }

    public function getData()
    {
        return $this->data;
    }

    public function buildQuery()
    {
        $sql = $this->getSql();
        if (substr($sql, -1) == ';') {
            $sql = substr($sql, 0, -1);
        }

        $groupBy = $this->getGroupBy();
        if ($groupBy !== null) {
            $sql .= ' group by ' . implode(',', (array)$groupBy);
        }

        $orderBy = $this->getOrderBy();
        if ($orderBy !== null) {
            $sql .= ' order by ' . implode(',', (array)$orderBy);
        }

        $limit = $this->getLimit();
        if ($limit !== null) {
            $sql .= ' limit ' . (int)$limit;
        }

        $offset = $this->getOffset();
        if ($offset !== null) {
            $sql .= ' offset ' . (int)$offset;
        }

        $sql = strtr($sql, array('[' => '`', ']' => '`'));

        return $sql;
    }
}
