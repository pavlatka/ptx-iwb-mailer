<?php namespace Ptx\Database\PtxPdo;

use \PDO;
use Ptx\Database\DatabaseException;
use Ptx\Database\Interfaces\QueryInterface;
use Ptx\Database\Interfaces\ResultInterface;

class Result implements ResultInterface
{
    private $pdo;
    private $query;
    private $errors;

    public function __construct(PDO $pdo)
    {
        $this->pdo   = $pdo;
    }

    public function setQuery(QueryInterface $query)
    {
        $this->query = $query;

        return $this;
    }

    public function run()
    {
        $query = $this->query->buildQuery();
        $pdo   = $this->pdo->prepare($query);

        $data  = $this->query->getData();
        foreach ($data as $key => $value) {
            $pdo->bindValue($key, $value);
        }

        $pdo->execute();

        $errors = $pdo->errorInfo();
        if (!empty($errors[1])) {
            throw new DatabaseException(
                'Database: ' . $errors[2] . ' [C:' . $errors[1] . ']'
            );
        }

        return $pdo;
    }

    public function fetch()
    {
        $statement = $this->run();
        $result    = $statement->fetch(PDO::FETCH_ASSOC);

        if ($result === false) {
            return array();
        }

        return (array)$result;
    }

    public function fetchAll()
    {
        $statement = $this->run();
        $result    = $statement->fetchAll(PDO::FETCH_ASSOC);

        if ($result === false) {
            return array();
        }

        return (array)$result;
    }

    public function fetchPairs($keyColumn, $valueColumn)
    {
        $pairs = array();
        $data  = $this->fetchAll();
        foreach ($data as $rows) {
            if (!array_key_exists($keyColumn, $rows) || array_key_exists($valueColumn, $rows)) {
                throw new DatabaseException(
                    'At leaset one of the requested columns does not exists.'
                );
            }

            $pairs[$row[$keyColumn]] = $row[$valueColumn];
        }

        return (array)$pairs;
    }
    public function fetchSingle()
    {
        $row = $this->fetch();
        if (count($row) === 0) {
            return null;
        }

        return array_values($row)[0];
    }
    public function limit($limit, $offset = null)
    {
        $this->query->setLimit($limit);
        if ($offset !== null) {
            $this->query->setOffset($offset);
        }

        return $this;
    }

    public function offset($offset)
    {
        $this->query->setOffset($offset);

        return $this;
    }

    public function orderBy($orderBy)
    {
        $this->query->setOrderBy($orderBy);

        return $this;
    }

    public function groupBy($groupBy)
    {
        $this->query->setGroupBy($groupBy);

        return $this;
    }

    public function getQuery()
    {
        return $this->query->buildQuery();
    }

    public function getErrors()
    {
        return $this->errors;
    }
}
