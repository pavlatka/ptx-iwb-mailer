<?php namespace Ptx\Database\Interfaces;

interface ResultInterface
{
    /**
     * Sets query for further use
     *
     * @param QueryInterface $query
     */
    public function setQuery(QueryInterface $query);

    /**
     * Process the query but
     * does not return anything.
     * Good for e.g. insert, update, delete queries
     */
    public function run();

    /**
     * Process query and return its result
     * as array
     *
     * @return array
     */
    public function fetch();

    /**
     * Process query and return all records
     * in array
     *
     * @return array
     */
    public function fetchAll();

    /**
     * Option to set how many records we want and also
     * optionally where to start
     *
     * @param int $limit
     * @param int $offset
     *
     * @return ResultInterface
     */
    public function limit($limit, $offset = null);

    /**
     * Possible to set up starting point
     *
     * @param int $offset
     *
     * @return ResultInterface
     */
    public function offset($offset);

    /**
     * Possible to set how the result should be sorted
     *
     * @param string|array $orderBy
     *
     * @return ResultInterface
     */
    public function orderBy($orderBy);

    /**
     * Possible to set up how the data should be groupped together
     *
     * @param string|array $groupBy
     *
     * @return ResultInterface
     */
    public function groupBy($groupBy);

    /**
     * Returns query as a string, e.g. for debug
     *
     * @return string
     */
    public function getQuery();
}
