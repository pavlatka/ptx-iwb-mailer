<?php namespace Ptx\Database\Interfaces;

interface ConnectionInterface
{
    /**
     * Process query and return instance of Result.
     *
     * @param string $sql - sql query
     * @param array  $params - additional params.
     *
     * @return Result
     */
    public function query($sql, array $params = array());
}
