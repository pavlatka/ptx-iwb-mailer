<?php namespace Ptx\Database\Interfaces;

interface QueryInterface
{
    public function setLimit($limit);
    public function setOffset($offset);
    public function setData($data);
    public function setGroupBy($groupBy);
    public function setOrderBy($orderBy);
    public function setSql($sql);

    public function getLimit();
    public function getOffset();
    public function getData();
    public function getGroupBy();
    public function getOrderBy();
    public function getSql();

    public function buildQuery();
}
