<?php
namespace Ptx\Api\Tests;

use Ptx\Api\Entity\ApiRequestEntity;

class ApiRequestEntityTest extends \PHPUnit_Framework_TestCase
{
    private $entity;

    protected function setUp()
    {
        $this->entity = new ApiRequestEntity();
    }

    protected function tearDown()
    {
        unset($this->entity);
    }

    public function testSetGetAuthToken()
    {
        $value = 'value';
        $this->entity->setAuthToken($value);

        $this->assertEquals($value, $this->entity->getAuthToken());
    }

    public function testSetGetVersion()
    {
        $value = 'value';
        $this->entity->setVersion($value);

        $this->assertEquals($value, $this->entity->getVersion());
    }

    public function testSetGetParams()
    {
        $value = 'value';
        $this->entity->setParams($value);

        $this->assertEquals($value, $this->entity->getParams());
    }

    public function testSetGetActionName()
    {
        $value = 'value';
        $this->entity->setActionName($value);

        $this->assertEquals($value, $this->entity->getActionName());
    }
}
