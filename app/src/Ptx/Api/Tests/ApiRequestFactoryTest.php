<?php
namespace Ptx\Api\Tests;

use Ptx\Api\Entity\ApiRequestEntity;
use \Ptx\Api\Factory\ApiRequestFactory;

class ApiRequestFactoryTest extends PHPUnit_Framework_TestCase
{
    private $factory;

    protected function setUp()
    {
        $this->factory = new ApiRequestFactory();
    }

    protected function tearDown()
    {
        unset($this->factory);
    }

    public function testCreateObjectCorrectEntity()
    {
        $entity = $this->factory->createObject(array());
        $isValid = $entity instanceof ApiRequestEntity;

        $this->assertTrue($isValid);
    }

    public function testCreateObjectDefaultMapping()
    {
        $entity = $this->factory->createObject(array());

        $this->assertEquals(null, $entity->getAuthToken());
        $this->assertEquals(null, $entity->getParams());
        $this->assertEquals(null, $entity->getVersion());
        $this->assertEquals(null, $entity->getActionName());
    }

    public function testCreateObjectProperMapping()
    {
        $data = array(
            'version'     => 1,
            'auth_token'  => 'auth_token',
            'action_name' => 'stats/demands',
            'params'      => array('params')
        );

        $entity = $this->factory->createObject($data);

        $this->assertEquals($data['auth_token'], $entity->getAuthToken());
        $this->assertEquals($data['params'], $entity->getParams());
        $this->assertEquals($data['version'], $entity->getVersion());
        $this->assertEquals($data['action_name'], $entity->getActionName());
    }
}
