<?php
namespace Ptx\Api\Tests;

use Ptx\Api\Controller\ApiController;

class ApiControllerTest extends \PHPUnit_Framework_TestCase
{
    private $factory;
    private $response;

    protected function setUp()
    {
        $this->factory  = $this->getMock('\Ptx\Api\Factory\ApiRequestFactory');
        $this->response = $this->getMock('\Ptx\Api\Response\ArrayResponse');
    }

    protected function tearDown()
    {
        unset($this->factory);
    }

    public function testPrepareApiRequestReturnProperEntity()
    {
        $factory = new \Ptx\Api\Factory\ApiRequestFactory();
        $controller = new ApiController($factory, $this->response);

        $apiRequest = $controller->prepareApiRequest('', '', array());

        $isValid = $apiRequest instanceof \Ptx\Api\Entity\ApiRequestEntity;
        $this->assertTrue($isValid);
    }

    public function dataTestPrepareApiRequestAuthTokenSetProperty()
    {
        return array(
            array('auth_token', 'auth_token'),
            array(array('auth_token'), 'auth_token'),
            array(array(), null)
        );
    }

    /**
     * @dataProvider dataTestPrepareApiRequestAuthTokenSetProperty
     */
    public function testPrepareApiRequestAuthTokenSetProperty($authToken, $expected)
    {
        $factory = new \Ptx\Api\Factory\ApiRequestFactory();
        $controller = new ApiController($factory, $this->response);

        $apiRequest = $controller->prepareApiRequest($authToken, '', array());

        $this->assertEquals($expected, $apiRequest->getAuthToken());
    }

    public function testPrepareApiRequestDataMapProperly()
    {
        $factory = new \Ptx\Api\Factory\ApiRequestFactory();
        $controller = new ApiController($factory, $this->response);

        $version   = 10;
        $authToken = 'auth_token';
        $actionName = 'action_name';
        $params = array(
            'version' => $version, 'type' => 'month');
        $expectedParams = array('type' => 'month');

        $apiRequest = $controller->prepareApiRequest($authToken, $actionName, $params);

        $this->assertEquals($version, $apiRequest->getVersion());
        $this->assertEquals($authToken, $apiRequest->getAuthToken());
        $this->assertEquals($actionName, $apiRequest->getActionName());
        $this->assertEquals($expectedParams, $apiRequest->getParams());
    }
}
