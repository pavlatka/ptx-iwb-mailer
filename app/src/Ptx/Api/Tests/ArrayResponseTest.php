<?php
namespace Ptx\Api\Tests;

use Ptx\Api\Response\ArrayResponse;

class ArrayResponseTest extends \PHPUnit_Framework_TestCase
{
    private $response;

    protected function setUp()
    {
        $this->response = new ArrayResponse();
    }

    protected function tearDown()
    {
        unset($this->response);
    }

    public function dataTestResponseProperResponse()
    {
        $scenarios = array();

        $data     = array();
        $expected = array(
            'code' => 404,
            'data' => array(
                'code'  => 404,
                'error' => 'No result has been found'
            )
        );
        $scenarios[] = array($data, $expected);

        $data = array(
            'error' => array(
                'code'    => 400,
                'message' => 'Error message'
            )
        );
        $expected =  array(
            'code' => 400,
            'data' => array(
                'code'  => 400,
                'error' => 'Error message'
            )
        );
        $scenarios[] = array($data, $expected);

        $data = array(
            'data' => array()
        );
        $expected =  array(
            'code' => 400,
            'data' => array(
                'code'  => 400,
                'error' => 'No result has been found'
            )
        );
        $scenarios[] = array($data, $expected);

        $data = array(
            'data' => array(
                'test_data', 'another_test_data'
            )
        );
        $expected = array(
            'code' => 200,
            'data' => array(
                'test_data', 'another_test_data'
            )
        );
        $scenarios[] = array($data, $expected);

        return (array)$scenarios;
    }

    /**
     * @dataProvider dataTestResponseProperResponse
     */
    public function testResponseProperResponse($data, $expected)
    {
        $data = array(
            'data' => array('test_data', 'another_test_data')
        );
        $expected = array('code' => 200, 'data' => $data['data']);

        $this->assertEquals($expected, $this->response->response($data));
    }
}
