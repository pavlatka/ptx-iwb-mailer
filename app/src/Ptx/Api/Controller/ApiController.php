<?php namespace Ptx\Api\Controller;

use Ptx\Api\Entity\ApiRequestEntity;
use Ptx\Api\Factory\ApiRequestFactory;
use Ptx\Api\Interfaces\ApiHandlerInterface;

class ApiController
{
    private $apiRequestFactory;
    private $response = array(
        'error' => array(),
        'data'  => array()
    );

    public function __construct(
        ApiRequestFactory $apiRequestFactory
    ) {

        $this->apiRequestFactory = $apiRequestFactory;
    }

    /**
     * Prepares entity for the apiRequest.
     *
     * @param string $authToken - auth token used for the call
     * @param string $actionName - name of the action called
     * @param array  $params - additional params
     *
     * @return ApiRequestEntity
     */
    public function prepareApiRequest($authToken, $actionName, array $params)
    {
        $version = null;
        if (array_key_exists('version', $params)) {
            $version = $params['version'];
            unset($params['version']);
        }

        if (is_array($authToken)) {
            if (!empty($authToken)) {
                $authToken = array_values($authToken)[0];
            } else {
                $authToken = null;
            }
        }

        $data = array(
            'params'      => $params,
            'version'     => $version,
            'auth_token'  => $authToken,
            'action_name' => $actionName
        );

        $entity = $this->apiRequestFactory->createObject($data);

        return $entity;
    }

    public function process(
        ApiRequestEntity $apiRequest,
        ApiHandlerInterface $handler
    ) {

        // If we need an authorization, but the authorization is not valid, return just errors.
        $authToken = $apiRequest->getAuthToken();
        if ($handler->authRequired() && !$this->validateAuthozation($authToken)) {
            return $this->response;
        }

        try {
            $response['data'] = $handler->process($apiRequest);
        } catch (\Exception $e) {
            $response['error'] = array(
                'code' => $e->getCode(),
                'message' => $e->getMessage()
            );
        }

        return (array)$response;
    }

    /**
     * Checks whether the authorization token is valid or not
     *
     * @param string $authToken - token to be checked.
     *
     * @return bool
     */
    protected function validateAuthozation($authToken)
    {
        $isValid   = true;
        $authToken = trim($authToken);
        if (empty($authToken)) {
            $isValid = false;
        } else {
            list($type, $token) = explode(' ', $authToken);
            $type = strtoupper($type);
            if ($type !== 'BASIC' || $token != 'ZWx1c3VhcmlvOnlsYWNsYXZl') {
                $isValid = false;
            }
        }

        if ($isValid === false) {
            $this->response['error'] = array(
                'code' => 404,
                'message' => 'You are not authorized to process this call.'
            );
        }

        return $isValid;
    }
}
