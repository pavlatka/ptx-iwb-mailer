<?php namespace Ptx\Api\Entity;

class ApiRequestEntity
{
    private $authToken;

    private $version;

    private $params;

    private $actionName;

    public function setAuthToken($authToken)
    {
        $this->authToken = $authToken;

        return $this;
    }

    public function getAuthToken()
    {
        return $this->authToken;
    }

    public function setVersion($version)
    {
        $this->version = $version;

        return $this;
    }

    public function getVersion()
    {
        return $this->version;
    }

    public function setParams($params)
    {
        $this->params = $params;

        return $this;
    }

    public function getParams()
    {
        return $this->params;
    }

    public function setActionName($actionName)
    {
        $this->actionName = $actionName;

        return $this;
    }

    public function getActionName()
    {
        return $this->actionName;
    }
}
