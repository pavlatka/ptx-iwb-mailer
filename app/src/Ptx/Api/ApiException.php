<?php namespace Ptx\Api;

class ApiException extends \Exception
{
    const ERROR_BAD_REQUEST = 400;
    const ERROR_NOT_FOUND   = 404;
}
