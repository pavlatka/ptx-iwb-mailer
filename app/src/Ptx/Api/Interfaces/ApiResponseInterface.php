<?php namespace Ptx\Api\Interfaces;

interface ApiResponseInterface
{
    /**
     * Responsible to response in certain way.
     *
     * @param array $data - data from the api call.
     *
     * @return mixed
     */
    public function response(array $data);
}
