<?php namespace Ptx\Api\Interfaces;

use Ptx\Api\Entity\ApiRequestEntity;

interface ApiHandlerInterface
{
    /**
     * Process call and returns result.
     *
     * @param ApiRequestEntity $apiRequest
     * @param array $options - additional options
     *
     * @return array
     */
    public function process(ApiRequestEntity $apiRequest, array $options = array());

    /**
     * Returns whehter the authorization is required
     * for the call
     *
     * @return bool
     */
    public function authRequired();
}
