<?php namespace Ptx\Api\Factory;

use Ptx\Api\Entity\ApiRequestEntity;

class ApiRequestFactory
{
    public function createObject(array $data)
    {
        $data += array(
            'params'      => null,
            'version'     => null,
            'auth_token'  => null,
            'action_name' => null
        );

        $entity = new ApiRequestEntity();
        $entity->setAuthToken($data['auth_token'])
            ->setParams($data['params'])
            ->setActionName($data['action_name'])
            ->setVersion($data['version']);

        return $entity;
    }
}
