<?php namespace Ptx\Api\Response;

use Ptx\Api\Interfaces\ApiResponseInterface;

class ArrayResponse implements ApiResponseInterface
{
    /**
     * Transforms data into the final
     * response format
     *
     * @param array $data
     *
     * @return array
     */
    public function response(array $data)
    {
        if (count($data) === 0) {
            return array(
                'code' => 404,
                'data' => array(
                    'code'  => 404,
                    'error' => 'No result has been found'
                )
            );
        }

        if (array_key_exists('error', $data)) {
            $error = $data['error'];

            return array(
                'code'  => $error['code'],
                'data' => array(
                    'code'  => $error['code'],
                    'error' => $error['message']
                )
            );
        }

        if (count($data['data']) === 0) {
            return array(
                'code' => 404,
                'data' => array(
                    'code'  => 404,
                    'error' => 'No result has been found'
                )
            );
        }

        return array(
            'code' => 200,
            'data' => $data['data']
        );
    }
}
