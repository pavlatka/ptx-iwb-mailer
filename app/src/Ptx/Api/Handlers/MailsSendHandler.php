<?php namespace Ptx\Api\Handlers;

use Ptx\Api\ApiException;
use Ptx\Api\Entity\ApiRequestEntity;
use Ptx\Mailer\Service\MailerService;
use Respect\Validation\Validator as v;

class MailsSendHandler extends AbstractHandler
{
    private $service;

    public function __construct(MailerService $service)
    {
        $this->service = $service;
    }

    public function process(
        ApiRequestEntity $apiRequest,
        array $params = array()
    ) {

        $params = $apiRequest->getParams();
        $this->validateParams($params);

        try {
            $userCode = $this->getUserAccessCode($apiRequest);

            $message = array(
                'send_to' => $params['send_to'],
                'subject' => $params['subject'],
                'message' => $this->collectMessageContent($params)
            );

            $result = $this->service->sendMessage4UserAccessToken($userCode, $message);

            if ($result) {
                $message['send_to'] = 'tomas@pavlatka.cz';
                $this->service->sendMessage4UserAccessToken($userCode, $message);
            }

            return array (
                'data' => array(
                    'result' => (int)$result
                )
            );
        } catch (ServiceException $e) {
            throw new ApiException(
                $e->getMessage(),
                $e->getCode()
            );
        }
    }

    /**
     * Collects message body
     *
     * @param array $data - data from the API call
     *
     * @return string
     */
    protected function collectMessageContent(array $data)
    {
        if (!empty($data['message'])) {
            return $data['message'];
        }

        if (!empty($data['message_url'])) {
            $messageUrl = $data['message_url'];
            $message    = trim(@file_get_contents($messageUrl));

            if (!empty($message) && $message != '__NO_CONTENT__') {
                return $message;
            }
        }

        throw new ApiException(
            'I could not retrieve message'
        );
    }

    /**
     * Validates params
     *
     * @throws ApiException
     */
    protected function validateParams(array $params)
    {
        if (empty($params['send_to'])) {
            throw new ApiException(
                'Send_to param is mandatory and cannot be empty.',
                ApiException::ERROR_BAD_REQUEST
            );
        } elseif (v::email()->validate($params['send_to']) !== true) {
            throw new ApiException(
                'Send_to param be valid email address.',
                ApiException::ERROR_BAD_REQUEST
            );
        }

        if (empty($params['subject'])) {
            throw new ApiException(
                'Subject param is mandatory and cannot be empty.',
                ApiException::ERROR_BAD_REQUEST
            );
        }

        if (empty($params['message']) && empty($params['message_url'])) {
            throw new ApiException(
                'Message or message_url must be provided.',
                ApiException::ERROR_BAD_REQUEST
            );
        }
    }

    /**
     * Returns information whether authorization
     * is required for the call
     *
     * @return bool
     */
    public function authRequired()
    {
        return false;
    }

    /**
     * Returns user access code
     *
     * @param ApiRequestEntity $apiRequest
     *
     * @return string
     */
    protected function getUserAccessCode(ApiRequestEntity $apiRequest)
    {
        $authToken = $apiRequest->getAuthToken();

        if ($authToken === null || strpos($authToken, ' ') === false) {
            return 'test';
        }

        list(, $accessCode) = explode(' ', $authToken);

        return trim($accessCode);
    }
}
