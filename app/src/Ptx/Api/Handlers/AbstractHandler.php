<?php namespace Ptx\Api\Handlers;

use Ptx\Api\Interfaces\ApiHandlerInterface;

abstract class AbstractHandler implements ApiHandlerInterface
{
    /**
     * Returns cache key from the params
     *
     * @param array $params - list of the params
     * @param string $prefix - prefix for the cache key
     *
     * @return string
     */
    protected function getCacheKeyFromParams(array $params, $prefix = null)
    {
        return $prefix . substr(sha1(serialize($params)), 0, 7);
    }
}
