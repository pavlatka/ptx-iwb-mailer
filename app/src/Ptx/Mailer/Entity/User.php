<?php
namespace Ptx\Mailer\Entity;

use Ptx\Mailer\LazyEntity\LazyUserSmtp;

class User
{
    private $id;
    private $name;
    private $stmp;

    public function setId($id)
    {
        $this->id = $id;

        return $this;
    }

    public function getId()
    {
        return $this->id;
    }

    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    public function getName()
    {
        return $this->name;
    }

    public function setSmtp(LazyUserSmtp $smtp)
    {
        $this->smtp = $smtp;

        return $this;
    }

    public function getSmtp()
    {
        return $this->smtp;
    }
}
