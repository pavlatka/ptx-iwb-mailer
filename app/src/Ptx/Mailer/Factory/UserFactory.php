<?php
namespace Ptx\Mailer\Factory;

use Ptx\Mailer\Entity\User;
use Ptx\Mailer\Factory\LazyEntityFactory;

class UserFactory
{
    private $lazyEntityFactory;

    public function __construct(
        LazyEntityFactory $factory
    ) {
        $this->lazyEntityFactory = $factory;
    }

    /**
     * Creates entity and maps its data properly
     *
     * @param array $data - data for the entity
     *
     * @return User
     */
    public function createObject(array $data)
    {
        $data += array(
            'id'   => null,
            'name' => null
        );

        $entity = new User();
        $entity
            ->setId($data['id'])
            ->setName($data['name']);

        $entity->setSmtp($this->lazyEntityFactory->createLazyLoadEntity('user_smtp', $data['id']));

        return $entity;
    }
}
