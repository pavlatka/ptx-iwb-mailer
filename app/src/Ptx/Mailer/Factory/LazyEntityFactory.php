<?php namespace Ptx\Mailer\Factory;

use Ptx\Mailer\LazyEntity\LazyUserSmtp;
use Ptx\Mailer\LazyEntity\MailerLazyEntityException;

class LazyEntityFactory
{
    private $container;

    public function __construct(\Slim\Container $container)
    {
        $this->container = $container;
    }

    /**
     * Creates new entity for the lazy entity
     *
     * @param string $entityName - name of the entity we need
     * @param int $id - Id of the
     *
     * @return AbstractUserLazyEntity
     */
    public function createLazyLoadEntity($entityName, $id)
    {
        switch ($entityName) {
            case 'user_smtp':
                return new LazyUserSmtp($id, $this->container->get('mailer_user_service'));
                break;
            default:
                throw new MailerLazyEntityException(
                    'The entity is unknown',
                    MailerLazyEntityException::ERROR_UNKNOWN_ENTITY
                );
                break;
        }
    }
}
