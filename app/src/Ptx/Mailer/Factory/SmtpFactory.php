<?php
namespace Ptx\Mailer\Factory;

use Ptx\Mailer\ValueObject\Smtp;

class SmtpFactory
{
    /**
     * Creates object and maps its data properly
     *
     * @param array $data - data for the object
     *
     * @return Smtp
     */
    public function createObject(array $data)
    {
        $data += array(
            'host'       => null,
            'username'   => null,
            'password'   => null,
            'from_email' => null,
            'from_text'  => null,
            'encryption' => null,
            'port'       => null
        );

        $object = new Smtp();
        $object
            ->setHost($data['host'])
            ->setUsername($data['username'])
            ->setPassword($data['password'])
            ->setFromEmail($data['from_email'])
            ->setFromText($data['from_text'])
            ->setEncryption($data['encryption'])
            ->setPort($data['port']);

        return $object;
    }
}
