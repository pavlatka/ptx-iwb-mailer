<?php namespace Ptx\Mailer\LazyEntity;

use Ptx\Base\Interfaces\LazyEntityExceptionInterface;

class MailerLazyEntityException extends \Exception implements LazyEntityExceptionInterface
{
}
