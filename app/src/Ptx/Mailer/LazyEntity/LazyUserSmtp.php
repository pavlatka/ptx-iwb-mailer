<?php namespace Ptx\Mailer\LazyEntity;

class LazyUserSmtp extends AbstractMailerLazyEntity
{
    /**
     * Returns address for the user
     *
     * @return Ptx\Mailer\ValueObject\Smtp
     */
    public function getData()
    {
        if ($this->data === null) {
            $this->data = $this->service->getSmtp4User($this->getId());
        }

        return $this->data;
    }
}
