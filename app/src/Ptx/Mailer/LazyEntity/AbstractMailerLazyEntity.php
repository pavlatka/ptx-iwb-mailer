<?php namespace Ptx\Mailer\LazyEntity;

use Ptx\Base\Interfaces\ServiceInterface;

abstract class AbstractMailerLazyEntity
{
    protected $id;
    protected $data;
    protected $service;

    public function __construct($id, ServiceInterface $service)
    {
        $this->id      = $id;
        $this->service = $service;
    }

    public function getId()
    {
        return $this->id;
    }

    /**
     * Returns data for the entity
     *
     * @return mixed
     */
    abstract public function getData();
}
