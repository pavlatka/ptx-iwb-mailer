<?php
namespace Ptx\Mailer\Tests;

use Ptx\Mailer\Factory\SmtpFactory;

class SmtpFactoryTest extends \PHPUnit_Framework_TestCase
{
    private $factory;

    protected function setUp()
    {
        $this->factory = new SmtpFactory();
    }

    protected function tearDown()
    {
        unset($this->factory);
    }

    public function testCreateObjectReturnProperEntityAndDefaultMapping()
    {
        $result = $this->factory->createObject(array());

        $isValid = $result instanceof \Ptx\Mailer\ValueObject\Smtp;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $result->getEncryption());
        $this->assertEquals(null, $result->getHost());
        $this->assertEquals(null, $result->getPort());
        $this->assertEquals(null, $result->getUsername());
        $this->assertEquals(null, $result->getPassword());
        $this->assertEquals(null, $result->getFromEmail());
        $this->assertEquals(null, $result->getFromText());
    }

    public function testCreateObjectMapsDataProperly()
    {
        $data = array(
            'host'       => 'host',
            'username'   => 'username',
            'password'   => 'password',
            'from_email' => 'from_email',
            'from_text'  => 'from_text',
            'encryption' => 'encryption',
            'port'       => 'port'
        );
        $result = $this->factory->createObject($data);

        $this->assertEquals($data['encryption'], $result->getEncryption());
        $this->assertEquals($data['host'], $result->getHost());
        $this->assertEquals($data['port'], $result->getPort());
        $this->assertEquals($data['username'], $result->getUsername());
        $this->assertEquals($data['password'], $result->getPassword());
        $this->assertEquals($data['from_email'], $result->getFromEmail());
        $this->assertEquals($data['from_text'], $result->getFromText());
    }
}
