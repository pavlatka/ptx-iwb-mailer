<?php
namespace Ptx\Mailer\Tests;

use Ptx\Mailer\Factory\UserFactory;

class UserFactoryTest extends \PHPUnit_Framework_TestCase
{
    private $factory;

    protected function setUp()
    {
        $smtp = $this->getMockBuilder('\Ptx\Mailer\LazyEntity\LazyUserSmtp')
            ->disableOriginalConstructor()
            ->getMock();

        $factory = $this->getMockBuilder('\Ptx\Mailer\Factory\LazyEntityFactory')
            ->disableOriginalConstructor()
            ->getMock();

        $factory->method('createLazyLoadEntity')->willReturn($smtp);

        $this->factory = new UserFactory($factory);
    }

    protected function tearDown()
    {
        unset($this->factory);
    }

    public function testCreateObjectReturnProperEntityAndDefaultMapping()
    {
        $result = $this->factory->createObject(array());

        $isValid = $result instanceof \Ptx\Mailer\Entity\User;
        $this->assertTrue($isValid);

        $this->assertEquals(null, $result->getId());
        $this->assertEquals(null, $result->getName());
    }

    public function testCreateObjectMapsDataProperly()
    {
        $data = array(
            'id'   => 'id',
            'name' => 'name'
        );
        $result = $this->factory->createObject($data);

        $this->assertEquals($data['id'], $result->getId());
        $this->assertEquals($data['name'], $result->getName());
    }
}
