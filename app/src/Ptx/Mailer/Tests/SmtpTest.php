<?php
namespace Ptx\Mailer\Tests;

use Ptx\Mailer\ValueObject\Smtp;

class SmtpTest extends \PHPUnit_Framework_TestCase
{
    private $object;

    protected function setUp()
    {
        $this->object = new Smtp();
    }

    protected function tearDown()
    {
        unset($this->object);
    }

    public function testSetGetEncryption()
    {
        $value = 10;
        $this->object->setEncryption($value);

        $this->assertEquals($value, $this->object->getEncryption());
    }

    public function testSetGetHost()
    {
        $value = 10;
        $this->object->setHost($value);

        $this->assertEquals($value, $this->object->getHost());
    }

    public function testSetGetPort()
    {
        $value = 10;
        $this->object->setPort($value);

        $this->assertEquals($value, $this->object->getPort());
    }

    public function testSetGetUsername()
    {
        $value = 10;
        $this->object->setUsername($value);

        $this->assertEquals($value, $this->object->getUsername());
    }

    public function testSetGetPassword()
    {
        $value = 10;
        $this->object->setPassword($value);

        $this->assertEquals($value, $this->object->getPassword());
    }

    public function testSetGetFromEmail()
    {
        $value = 10;
        $this->object->setFromEmail($value);

        $this->assertEquals($value, $this->object->getFromEmail());
    }

    public function testSetGetFromText()
    {
        $value = 10;
        $this->object->setFromText($value);

        $this->assertEquals($value, $this->object->getFromText());
    }

    public function testGetFromReturnStringWhenFromTextIsEmpty()
    {
        $this->object->setFromEmail('test@mail.com');
        $this->assertEquals('test@mail.com', $this->object->getFrom());

        $this->object->setFromText('');
        $this->assertEquals('test@mail.com', $this->object->getFrom());
    }

    public function testGetFromReturnsArrayWhenFromTextIsNotEmpty()
    {
        $this->object->setFromEmail('test@mail.com');
        $this->object->setFromText('TestUser');

        $this->assertEquals(array('test@mail.com' => 'TestUser'), $this->object->getFrom());
    }
}
