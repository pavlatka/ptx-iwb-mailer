<?php
namespace Ptx\Mailer\Tests;

use Ptx\Mailer\Factory\LazyEntityFactory;

class LazyEntityFactoryTest extends \PHPUnit_Framework_TestCase
{
    private $factory;

    protected function setUp()
    {
        $service = $this->getMockBuilder('\Ptx\Mailer\Service\UserService')
            ->disableOriginalConstructor()
            ->getMock();

        $container = $this->getMockBuilder('\Slim\Container')
            ->disableOriginalConstructor()
            ->getMock();

        $container->method('get')->willReturn($service);

        $this->factory = new LazyEntityFactory($container);
    }

    protected function tearDown()
    {
        unset($this->factory);
    }

    /**
     * @expectedException \Ptx\Mailer\LazyEntity\MailerLazyEntityException
     */
    public function testCreateLazyEntityThrowsExceptionOnUnknownEntity()
    {
        $this->factory->createLazyLoadEntity('test', 1);
    }

    public function testCreateLazyEntityReturnsCorrectEntity()
    {
        $result = $this->factory->createLazyLoadEntity('user_smtp', 1);

        $isValid = $result instanceof \Ptx\Mailer\LazyEntity\LazyUserSmtp;
        $this->assertTrue($isValid);
    }
}
