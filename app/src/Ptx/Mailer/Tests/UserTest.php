<?php
namespace Ptx\Mailer\Tests;

use Ptx\Mailer\Entity\User;

class UserTest extends \PHPUnit_Framework_TestCase
{
    private $entity;

    protected function setUp()
    {
        $this->entity = new User();
    }

    protected function tearDown()
    {
        unset($this->entity);
    }

    public function testSetGetId()
    {
        $value = 10;
        $this->entity->setId($value);

        $this->assertEquals($value, $this->entity->getId());
    }

    public function testSetGetName()
    {
        $value = 10;
        $this->entity->setName($value);

        $this->assertEquals($value, $this->entity->getName());
    }
}
