<?php
namespace Ptx\Mailer\ValueObject;

class Smtp
{
    private $host;
    private $port;
    private $fromText;
    private $fromEmail;
    private $username;
    private $password;
    private $encryption;

    public function setHost($host)
    {
        $this->host = $host;

        return $this;
    }

    public function getHost()
    {
        return $this->host;
    }

    public function setPort($port)
    {
        $this->port = $port;

        return $this;
    }

    public function getPort()
    {
        return $this->port;
    }

    public function setUsername($username)
    {
        $this->username = $username;

        return $this;
    }

    public function getUsername()
    {
        return $this->username;
    }

    public function setPassword($password)
    {
        $this->password = $password;

        return $this;
    }

    public function getPassword()
    {
        return $this->password;
    }

    public function setFromEmail($fromEmail)
    {
        $this->fromEmail = $fromEmail;

        return $this;
    }

    public function getFromEmail()
    {
        return $this->fromEmail;
    }

    public function setFromText($fromText)
    {
        $this->fromText = $fromText;

        return $this;
    }

    public function getFromText()
    {
        return $this->fromText;
    }

    public function setEncryption($encryption)
    {
        $this->encryption = $encryption;

        return $this;
    }

    public function getEncryption()
    {
        return $this->encryption;
    }

    public function getFrom()
    {
        $fromText  = $this->getFromText();
        $fromEmail = $this->getFromEmail();

        if (!empty($fromText)) {
            return array($fromEmail => $fromText);
        }

        return $fromEmail;
    }
}
