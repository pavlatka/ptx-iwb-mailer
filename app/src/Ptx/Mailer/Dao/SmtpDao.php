<?php
namespace Ptx\Mailer\Dao;

use Ptx\Database\Interfaces\ConnectionInterface;

class SmtpDao
{
    private $database;

    public function __construct(
        ConnectionInterface $database
    ) {
        $this->database = $database;
    }

    /**
     * Returns information about smtp setting for user
     *
     * @param int $userId - id of the user
     *
     * @return array
     */
    public function getSmtp4User($userId)
    {
        $sql = '
            select
                smtp_host host,
                smtp_username username,
                smtp_password password,
                smtp_from from_email,
                smtp_from_text from_text,
                smtp_port port,
                smtp_encryption encryption
            from [users]
            where
                id = :user_id';
        $result = $this->database->query($sql, array(
            ':user_id' => $userId))->limit(1);

        return $result->fetch();
    }
}
