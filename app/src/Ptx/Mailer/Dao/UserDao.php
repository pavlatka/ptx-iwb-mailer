<?php
namespace Ptx\Mailer\Dao;

use Ptx\Database\Interfaces\ConnectionInterface;

class UserDao
{
    private $database;

    public function __construct(
        ConnectionInterface $database
    ) {
        $this->database = $database;
    }

    /**
     * Returns information about the user
     *
     * @param int $userId - id of the user
     *
     * @return array
     */
    public function getUserById($userId)
    {
        $sql = '
            select id, name
            from [users]
            where
                id = :user_id';
        $result = $this->database->query($sql, array(
            ':user_id' => $userId))->limit(1);

        return $result->fetch();
    }

    /**
     * Returns information about the user
     *
     * @param int $accessToken - access token
     *
     * @return array
     */
    public function getUserByAccessToken($accessToken)
    {
        $sql = '
            select id, name
            from [users]
            where
                access_token = :token';
        $result = $this->database->query($sql, array(
            ':token' => $accessToken))->limit(1);

        return $result->fetch();
    }
}
