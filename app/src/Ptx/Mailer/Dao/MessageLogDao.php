<?php
namespace Ptx\Mailer\Dao;

use \Swift_Message;
use Ptx\Mailer\Entity\User;
use Ptx\Database\Interfaces\ConnectionInterface;

class MessageLogDao
{
    private $database;

    public function __construct(
        ConnectionInterface $database
    ) {
        $this->database = $database;
    }

    /**
     * Saves new log for the message
     *
     * @param User $user - entity of the user who sent the message
     * @param \Swift_Message $message - message which was sent
     *
     * @return bool
     */
    public function saveMessageLog(
        User $user,
        \Swift_Message $message
    ) {

        $sql = '
            insert into [message_logs] (user_id, send_to, subject, body)
            values (:user_id, :send_to, :subject, :message)';

        $data = array(
            ':user_id' => $user->getId(),
            ':subject' => $message->getSubject(),
            ':send_to' => $message->getTo(),
            ':message' => $message->getBody()
        );

        foreach ($data as $key => $value) {
            if (is_array($value)) {
                $data[$key] = json_encode($value);
            }
        }

        $this->database->query($sql, $data)->run();
    }
}
