<?php
namespace Ptx\Mailer\Repository;

use \Swift_Message;
use Ptx\Mailer\Entity\User;
use Ptx\Mailer\Dao\MessageLogDao;

class MessageLogRepository
{
    private $dao;

    public function __construct(MessageLogDao $dao)
    {
        $this->dao = $dao;
    }

    /**
     * Saves new log for the message
     *
     * @param User $user - entity of the user who sent the message
     * @param \Swift_Message $message - message which has been sent
     */
    public function saveMessageLog(
        User $user,
        \Swift_Message $message
    ) {
        $this->dao->saveMessageLog($user, $message);
    }
}
