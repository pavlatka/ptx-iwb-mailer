<?php
namespace Ptx\Mailer\Repository;

use Ptx\Mailer\Dao\SmtpDao;
use Ptx\Mailer\Factory\SmtpFactory;

class SmtpRepository
{
    private $dao;
    private $factory;

    public function __construct(
        SmtpDao $dao,
        SmtpFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    /**
     * Returns user entity base on its id
     *
     * @param int $userId - id of the user
     *
     * @return Ptx\Mailer\ValueObject\Smtp
     *
     * @throws UserRepositoryException
     */
    public function getSmtp4User($userId)
    {
        $user = $this->dao->getSmtp4User($userId);
        if (count($user) === 0) {
            throw new UserRepositoryException(
                'There is smtp settings for such id. [I:' . $userId . ']',
                UserRepositoryException::ERROR_NOT_FOUND
            );
        }

        return $this->factory->createObject($user);
    }
}
