<?php
namespace Ptx\Mailer\Repository;

use Ptx\Base\Interfaces\RepositoryExceptionInterface;

class UserRepositoryException extends \Exception implements RepositoryExceptionInterface
{
}
