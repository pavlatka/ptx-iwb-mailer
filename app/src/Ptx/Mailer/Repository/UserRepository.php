<?php
namespace Ptx\Mailer\Repository;

use Ptx\Mailer\Dao\UserDao;
use Ptx\Mailer\Factory\UserFactory;

class UserRepository
{
    private $dao;
    private $factory;

    public function __construct(
        UserDao $dao,
        UserFactory $factory
    ) {
        $this->dao     = $dao;
        $this->factory = $factory;
    }

    /**
     * Returns user entity base on its id
     *
     * @param int $userId - id of the user
     *
     * @return User
     *
     * @throws UserRepositoryException
     */
    public function getUserById($userId)
    {
        $user = $this->dao->getUserById($userId);
        if (count($user) === 0) {
            throw new UserRepositoryException(
                'There is not user for such id. [I:' . $userId . ']',
                UserRepositoryException::ERROR_NOT_FOUND
            );
        }

        return $this->factory->createObject($user);
    }

    /**
     * Returns user entity base on its code
     *
     * @param int $accessToken - id of the user
     *
     * @return User
     *
     * @throws UserRepositoryException
     */
    public function getUserByAccessToken($accessToken)
    {
        $user = $this->dao->getUserByAccessToken($accessToken);
        if (count($user) === 0) {
            throw new UserRepositoryException(
                'There is not user for such access token. [T:' . $accessToken . ']',
                UserRepositoryException::ERROR_NOT_FOUND
            );
        }

        return $this->factory->createObject($user);
    }
}
