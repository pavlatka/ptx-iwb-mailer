<?php
namespace Ptx\Mailer\Service;

use \Swift_Mailer;
use \Swift_Message;
use \Swift_SmtpTransport;
use \Ptx\Mailer\Entity\User;
use \Ptx\Mailer\Repository\MessageLogRepository;

class MailerService
{
    private $userService;
    private $messageLogRepository;

    public function __construct(
        UserService $userService,
        MessageLogRepository $messageLogRepository
    ) {
        $this->userService          = $userService;
        $this->messageLogRepository = $messageLogRepository;
    }

    /**
     * Send a message for the user identified by his id
     *
     * @param int $userId - id of the user
     * @param array $message - data for the message
     *
     * @return bool
     */
    public function sendMessage4User($userId, array $message)
    {
        try {
            $user    = $this->userService->getUserById($userId);
            $message = $this->prepareMessage($message);

            return $this->sendMessage($user, $message);
        } catch (\Exception $e) {
        }
    }

    /**
     * Send a message for the user identified by his access token
     *
     * @param string $accessToken - access token
     * @param array $message - data for the message
     *
     * @return bool
     */
    public function sendMessage4UserAccessToken($accessToken, array $message)
    {
        try {
            $user    = $this->userService->getUserByAccessToken($accessToken);
            $message = $this->prepareMessage($message);

            return $this->sendMessage($user, $message);
        } catch (\Exception $e) {
            return false;
        }
    }

    /**
     * Responsible to send a message for the use
     *
     * @param User $user - user who is sending the message
     * @param \Swift_Message $message - message which is gonna be sent
     *
     * @return bool
     *
     * @throws UserServiceException
     */
    protected function sendMessage(User $user, \Swift_Message $message)
    {
        $smtp = $user->getSmtp()->getData();

        $transport = \Swift_SmtpTransport::newInstance();
        $transport
            ->setHost($smtp->getHost())
            ->setPort($smtp->getPort())
            ->setEncryption($smtp->getEncryption())
            ->setUsername($smtp->getUsername())
            ->setPassword($smtp->getPassword());

        $message->setFrom($smtp->getFrom());

        $mailer = \Swift_Mailer::newInstance($transport);

        $result = $mailer->send($message);
        if ($result) {
//            $this->messageLogRepository->saveMessageLog($user, $message);
        }

        return (bool)$result;
    }

    /**
     * Prepares message
     *
     * @param array $data - data for the message
     *
     * @retun \Swift_Message
     */
    protected function prepareMessage(array $data)
    {
        $data += array(
            'subject' => 'Test Message',
            'send_to' => 'test@email.com',
            'message' => 'Test Message'
        );

        $body      = $data['message'];
        $bodyPlain = \Html2Text\Html2Text::convert($body);

        $message = \Swift_Message::newInstance();
        $message
            ->setSubject($data['subject'])
            ->setTo($data['send_to'])
            ->setBody($body, 'text/html')
            ->addPart($bodyPlain, 'text/plain');

        return $message;
    }
}
