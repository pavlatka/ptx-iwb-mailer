<?php
namespace Ptx\Mailer\Service;

use Ptx\Base\Interfaces\ServiceExceptionInterface;

class UserServiceException extends \Exception implements ServiceExceptionInterface
{
}
