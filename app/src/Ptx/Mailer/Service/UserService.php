<?php
namespace Ptx\Mailer\Service;

use Ptx\Base\Interfaces\ServiceInterface;
use Ptx\Mailer\Repository\UserRepository;
use Ptx\Mailer\Repository\SmtpRepository;

class UserService implements ServiceInterface
{
    private $userRepository;
    private $userSmtpRepository;

    public function __construct(
        UserRepository $userRepository,
        SmtpRepository $smtpRepository
    ) {
        $this->userRepository = $userRepository;
        $this->smtpRepository = $smtpRepository;
    }

    /**
     * Returns user entity for userId
     *
     * @param int $userId - id of the user
     *
     * @return \Ptx\Mailer\Entity\User
     *
     * @throws UserServiceException
     */
    public function getUserById($userId)
    {
        try {
            return $this->userRepository->getUserById($userId);
        } catch (UserRepositoryException $e) {
            throw new UserServiceException($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            throw new UserServiceException(
                $e->getCode() .': ' .  $e->getMessage(),
                UserServiceException::ERROR_RUNTIME_ERROR
            );
        }
    }

    /**
     * Returns user entity for access token
     *
     * @param int $accessToken - id of the user
     *
     * @return \Ptx\Mailer\Entity\User
     *
     * @throws UserServiceException
     */
    public function getUserByAccessToken($accessToken)
    {
        try {
            return $this->userRepository->getUserByAccessToken($accessToken);
        } catch (UserRepositoryException $e) {
            throw new UserServiceException($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            throw new UserServiceException(
                $e->getCode() .': ' .  $e->getMessage(),
                UserServiceException::ERROR_RUNTIME_ERROR
            );
        }
    }

    /**
     *
     * Returns smtp settings for the user
     *
     * @param int $userId - id of the user
     *
     * @return \Ptx\Mailer\ValueObject\Smtp
     *
     * @throws UserServiceException
     */
    public function getSmtp4User($userId)
    {
        try {
            return $this->smtpRepository->getSmtp4User($userId);
        } catch (UserRepositoryException $e) {
            throw new UserServiceException($e->getMessage(), $e->getCode());
        } catch (\Exception $e) {
            throw new UserServiceException(
                $e->getCode() .': ' .  $e->getMessage(),
                UserServiceException::ERROR_RUNTIME_ERROR
            );
        }
    }
}
