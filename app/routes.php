<?php

use \Slim\Http\Request;
use \Slim\Http\Response;
use \Uvch\Api\Handlers\CallHandler;

$app->get('/', function () {
    return 'Silence is golden!';
});

$routeFiles = glob(__DIR__ .'/routes/*.php');
foreach ($routeFiles as $file) {
    require_once $file;
}
