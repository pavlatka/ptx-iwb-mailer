<?php

$container = $app->getContainer();

$container['db_pdo'] = function ($c) {
    $settings = array(
        'host'     => getEnv('MYSQL_HOST') ?: 'localhost',
        'username' => getEnv('MYSQL_USER') ?: 'root',
        'password' => getEnv('MYSQL_PASSWORD') ?: 'root',
        'database' => getEnv('MYSQL_DATABASE') ?: 'mailer',
        'charset'  => getEnv('MYSQL_CHARSET') ?: 'utf8',
    );

    $dsn = sprintf(
        'mysql:dbname=%s;host=%s;charset=%s',
        $settings['database'],
        $settings['host'],
        $settings['charset']
    );
    return new \PDO($dsn, $settings['username'], $settings['password']);
};

$container['db_result'] = function ($c) {
    return new \Ptx\Database\PtxPdo\Result($c['db_pdo']);
};

$container['db_connection'] = function ($c) {
    return new \Ptx\Database\PtxPdo\Connection($c['db_result']);
};

$container['base_identity_mapper'] = function ($c) {
    return new \Ptx\Base\Mapper\IdentityMapper();
};

$dependenciesFiles = glob(__DIR__ .'/dependencies/*.php');
foreach ($dependenciesFiles as $file) {
    require_once $file;
}
