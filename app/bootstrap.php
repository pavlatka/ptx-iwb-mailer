<?php

require_once __DIR__ . '/../vendor/autoload.php';

$errorReporting = getEnv('ERROR_REPORTING') ?: O;

$app = new \Slim\App(array(
    'settings' => array(
        'displayErrorDetails' => $errorReporting
    )
));

require_once __DIR__ . '/routes.php';
require_once __DIR__ . '/dependencies.php';

$app->run();
